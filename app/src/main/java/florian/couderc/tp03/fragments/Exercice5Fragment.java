package florian.couderc.tp03.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import florian.couderc.tp03.R;

public class Exercice5Fragment extends Fragment {

    private EditText console;
    private int[] array = {1, 15, -3, 0, 8, 7, 4, -2, 28, 7, -1, 17, 2, 3, 0, 14, -4};

    public Exercice5Fragment() {}

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exercice5, container, false);
        console = (EditText) view.findViewById(R.id.exo5_tableau);
        Button buttonFibo = view.findViewById(R.id.exo5_button);
        EditText inputRand = view.findViewById(R.id.exo5_input);
        buttonFibo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!inputRand.getText().toString().equals("")) {
                    console.setText(fibonacci(Integer.parseInt(inputRand.getText().toString())).toString());
                }
            }
        });
        return view;
    }

    private ArrayList<Integer> fibonacci (int N) {
        ArrayList<Integer> fibo = new ArrayList<Integer>();
        fibo.add(0);
        fibo.add(1);
        for (int i = 2; i < N+1; i++) {
            fibo.add(fibo.get(i-1) + fibo.get(i-2));
        }
        return fibo;
    }
}
