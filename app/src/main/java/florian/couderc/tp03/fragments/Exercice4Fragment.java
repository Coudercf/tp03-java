package florian.couderc.tp03.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import java.util.Arrays;

import florian.couderc.tp03.R;

public class Exercice4Fragment extends Fragment {

    private int[] array = {};

    public Exercice4Fragment() {}

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exercice4, container, false);
        Button addButton = (Button) view.findViewById(R.id.exo4_button);
        EditText inputNumber = (EditText) view.findViewById(R.id.exo4_input);
        EditText tableau = (EditText) view.findViewById(R.id.exo4_tableau);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int add = Integer.parseInt(inputNumber.getText().toString());
                array = addElement(array, add);
                tableau.setText("");
                inputNumber.setText("");
                for (int value : array) {
                    tableau.setText(tableau.getText().toString() + value + ", ");
                }
            }
        });
        return view;
    }

    private int[] addElement(int[] a, int e) {
        a  = Arrays.copyOf(a, a.length + 1);
        a[a.length - 1] = e;
        return a;
    }
}
