package florian.couderc.tp03.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import florian.couderc.tp03.R;

public class Exercice1Fragment extends Fragment {

    private EditText console;
    private int[] array = {1, 15, -3, 0, 8, 7, 4, -2, 28, 7, -1, 17, 2, 3, 0, 14, -4};

    public Exercice1Fragment() {}

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exercice1, container, false);
        console = ((EditText) view.findViewById(R.id.exo1_console));
        Button button1 = (Button) view.findViewById(R.id.ex1_button1);
        Button button2 = (Button) view.findViewById(R.id.ex1_button2);
        Button button3 = (Button) view.findViewById(R.id.ex1_button3);
        Button button4 = (Button) view.findViewById(R.id.ex1_button4);
        Button button5 = (Button) view.findViewById(R.id.ex1_button5);
        Button button6 = (Button) view.findViewById(R.id.ex1_button6);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                console.setText("");
                display_array();
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                console.setText("");
                display_reversed_array();
            }
        });
        final EditText more_than = (EditText) view.findViewById(R.id.more_than);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                console.setText("");
                String moreStr = more_than.getText().toString();
                System.out.println(moreStr);
                if (!moreStr.equals("")) {
                    int more = Integer.parseInt(more_than.getText().toString());
                    display_more_than(more);
                }
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                console.setText("");
                display_even_numbers();
            }
        });
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                console.setText("");
                display_biggest_number();
            }
        });
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                console.setText("");
                display_smallest_number();
            }
        });
        return view;
    }

    private void display_array() {
        console.setText("Affichage du tableau :\n");
        for (int value : this.array) {
            console.setText(console.getText().toString() + value + ", ");
        }
    }

    private void display_reversed_array() {
        console.setText("Affichage inverse du tableau :\n");
        for (int i = this.array.length - 1; i > 0; i--) {
            console.setText(console.getText().toString() + this.array[i] + ", ");
        }
    }

    private void display_more_than(int more) {
        console.setText("Affichage des nombres plus grand que " + more + "\n");
        for (int value : this.array) {
            if (value > more) {
                console.setText(console.getText().toString() + value + ", ");
            }
        }
    }

    private void display_even_numbers() {
        console.setText("Affichage des nombres paires :\n");
        for (int value : this.array) {
            if (value % 2 == 0) {
                console.setText(console.getText().toString() + value + ", ");
            }
        }
    }

    private void display_biggest_number() {
        console.setText("Affichage du nombre le plus grand :\n");
        int max = 0;
        for (int value : this.array) {
            if (value > max) {
                max = value;
            }
        }
        console.setText(console.getText().toString() + "= " + max);
    }

    private void display_smallest_number() {
        console.setText("Affichage du nombre le plus petit :\n");
        double min = Double.POSITIVE_INFINITY;
        for (int value : this.array) {
            if (value < min) {
                min = value;
            }
        }
        console.setText(console.getText().toString() + "= " + min);
    }
}
