package florian.couderc.tp03.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import java.util.Arrays;

import florian.couderc.tp03.R;

public class Exercice3Fragment extends Fragment {

    private EditText console;
    private int[] array = {1, 15, -3, 0, 8, 7, 4, -2, 28, 7, -1, 17, 2, 3, 0, 14, -4};

    public Exercice3Fragment() {}

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exercice3, container, false);
        console = ((EditText) view.findViewById(R.id.exo3_console));
        Button button1 = (Button) view.findViewById(R.id.ex3_button1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                console.setText("");
                display_sorted_array();
            }
        });
        return view;
    }

    private void display_sorted_array() {
        Arrays.sort(array);
        console.setText("Affichage du tableau trié avec Arrays :\n");
        for (int value : this.array) {
            console.setText(console.getText().toString() + value + ", ");
        }
    }
}
