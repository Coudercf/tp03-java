package florian.couderc.tp03.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.stream.IntStream;

import florian.couderc.tp03.R;

public class Exercice2Fragment extends Fragment {

    private EditText console;
    private int[] array = {1, 15, -3, 0, 8, 7, 4, -2, 28, 0, 7, -1, 17, 2, 3, 2, 0, 14, -4};

    public Exercice2Fragment() {}

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exercice2, container, false);
        console = ((EditText) view.findViewById(R.id.exo2_console));
        Button button1 = (Button) view.findViewById(R.id.ex2_button1);
        Button button2 = (Button) view.findViewById(R.id.ex2_button2);
        Button button3 = (Button) view.findViewById(R.id.ex2_button3);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                console.setText("");
                display_average();
            }
        });
        final EditText index_of = (EditText) view.findViewById(R.id.index_of);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                console.setText("");
                String indexStr = index_of.getText().toString();
                if (!indexStr.equals("")) {
                    int index = Integer.parseInt(index_of.getText().toString());
                    display_index_of(index);
                }
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                console.setText("");
                display_more_than_one_numbers();
            }
        });
        return view;
    }

    private void display_average() {
        console.setText("Affichage de la moyenne :\n");
        int sum = IntStream.of(array).sum();
        console.setText(console.getText().toString() + "Moyenne = " + sum / this.array.length);
    }

    public void display_index_of(int index) {
        console.setText("Affichage de(s) l'index de " + index + " :\n= ");
        for (int i = 0; i < array.length; i++) {
            if (array[i] == index) {
                console.setText(console.getText().toString() + i + ", ");
            }
        }
    }

    public void display_more_than_one_numbers() {
        console.setText("Affichage des doublons :\n");
        ArrayList<Integer> listDoublon = new ArrayList<Integer>();
        for (int i = 0; i < this.array.length; i++) {
            int number_of = 0;
            for (int j = 0; j < this.array.length; j++) {
                if (this.array[i] == this.array[j] && !listDoublon.contains(j)) {
                    number_of++;
                    listDoublon.add(j);
                }
            }
            if (number_of > 1) {
                console.setText(console.getText().toString() + "" +
                        "Il y a " + number_of + " de " + this.array[i] + "\n");
            }
        }
    }
}
