package florian.couderc.tp03;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import florian.couderc.tp03.fragments.Credits;
import florian.couderc.tp03.fragments.Exercice1Fragment;
import florian.couderc.tp03.fragments.Exercice2Fragment;
import florian.couderc.tp03.fragments.Exercice3Fragment;
import florian.couderc.tp03.fragments.Exercice4Fragment;
import florian.couderc.tp03.fragments.Exercice5Fragment;

import net.steamcrafted.materialiconlib.MaterialDrawableBuilder;

public class MainActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        setViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        Drawable yourDrawable = MaterialDrawableBuilder.with(getApplicationContext()) // provide a context
                .setIcon(MaterialDrawableBuilder.IconValue.STAR_CIRCLE) // provide an icon
                .setColorResource(R.color.colorAccent) // set the icon color
                .setToActionbarSize() // set the icon size
                .build(); // Finally call build
        tabLayout.getTabAt(5).setIcon(yourDrawable);
    }

    private void setViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Exercice1Fragment(), "Exo 1");
        adapter.addFragment(new Exercice2Fragment(), "Exo 2");
        adapter.addFragment(new Exercice3Fragment(), "Exo 3");
        adapter.addFragment(new Exercice4Fragment(), "Exo 4");
        adapter.addFragment(new Exercice5Fragment(), "Exo 5");
        adapter.addFragment(new Credits(), "");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
